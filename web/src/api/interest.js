import axios from 'axios'
import {API_URL} from "../constants/general";

export const getInterest = ({ borrowingAmount, noOfMonths }) => {
    return axios.get(`${API_URL}/interest?amount=${borrowingAmount}&numMonths=${noOfMonths}`)
        .then(response => {
            return response.data
        })
        .catch(error => {
            return error
        })
};
