import { put, call, takeEvery, all } from 'redux-saga/effects'
import {
    getInterest
} from '../api/interest'
import types from '@constants/action-types'
import * as actions from '@actions/interest'

export const handleGetInterestRequest = function * ({ payload }) {
    try {
        const response = yield call(getInterest, payload);
        if (response.isAxiosError) return yield put(actions.getInterestFailure(response.message));
        yield put(actions.getInterestSuccess(response))
    } catch (e) {
        yield put(actions.getInterestFailure(e))
    }
};

export default all([
    takeEvery(types.GET_INTEREST_REQUEST, handleGetInterestRequest)
])
