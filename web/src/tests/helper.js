import * as React from "react";
import {createStore} from 'redux'
import {Provider} from 'react-redux'
import {render as rtlRender } from '@testing-library/react'
import { MemoryRouter } from "react-router-dom";

import rootReducer from '../reducers'

const initialReducerState = {};

function render(
    ui,
    {
        initialState = initialReducerState,
        store = createStore(rootReducer, initialState),
        ...renderOptions
    } = {},
) {
    function Wrapper({children}) {
        return (
            <Provider store={store}>
                <MemoryRouter>
                    {children}
                </MemoryRouter>
            </Provider>
            )
    }
    return rtlRender(ui, {wrapper: Wrapper, ...renderOptions})
}

// re-export everything
export * from '@testing-library/react'

// override render method
export { render }
