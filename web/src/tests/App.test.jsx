import React from 'react';
import { render } from './helper'

import App from '../App';

describe('Initial App render', () => {
  const { asFragment } = render(<App />);
  test('should render app', async () => {

    expect(asFragment()).toMatchSnapshot();
  });
});
