import { render, fireEvent, screen } from '../helper'

import React from "react";
import LoanSelector from "@containers/LoanSelector";

describe('LoanSelector Component', () => {

    const setup = () => {
        return render(<LoanSelector/>, {
            initialState: {
                interest: {
                    isLoading: false,
                    error: null,
                    data: {
                        monthlyPayment: {
                            amount: 10
                        },
                        totalInterest: {
                            amount: 10
                        },
                    }
                }
            }
        });
    };

    it('should render with given state from Redux store', async () => {
        const {asFragment} = setup();
        const interestAmount = await screen.findByText('Interest: £10');
        const monthlyPaymentAmount = await screen.findByText('Monthly Repayments: £10');
        expect(interestAmount).toBeInTheDocument();
        expect(monthlyPaymentAmount).toBeInTheDocument();
        expect(asFragment()).toMatchSnapshot()
    });

    it('should dispatch an action on when slider value changes', async () => {
        const { container } = setup();
        const sliderInput = container.querySelector(`input[name=borrowingAmount]`);
        fireEvent.change(sliderInput, { target: { value: '0' } });
    });
});
