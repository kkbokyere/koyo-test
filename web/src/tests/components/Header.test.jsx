import React from 'react';
import { render } from '../helper'

import Header from '@components/Header';

describe('Header', () => {
    const { asFragment } = render( <Header />);
    test('should Header', async () => {
        expect(asFragment()).toMatchSnapshot();
    });
});
