import React from 'react';
import { render } from '../helper'
import Layout from "@components/Layout";

describe('Layout', () => {
    const { asFragment, getByText, container } = render( <Layout>
        <h1>Some Heading Text</h1>
    </Layout>);
    test('should Layout', async () => {
        const headingText = getByText('Some Heading Text');
        expect(headingText).toBeInTheDocument();
        expect(asFragment()).toMatchSnapshot();
    });
});
