import React from 'react';
import { render } from '../helper'

import ContentBox from '@components/ContentBox';

describe('ContentBox', () => {
    const { asFragment, getByText } = render( <ContentBox>
        <h1>Some Heading Text</h1>
    </ContentBox>);
    test('should ContentBox', async () => {
        const headingText = getByText('Some Heading Text');
        expect(headingText).toBeInTheDocument();
        expect(asFragment()).toMatchSnapshot();
    });
});
