import { connect } from 'react-redux'
import { getInterestRequest } from '@actions/interest'
import LoanSelector from "./LoanSelector";
const mapStateToProps = ({ interest }) => ({
    interest
});

const mapDispatchToProps = dispatch => ({
    handleGetInterestRequest: data => {
        dispatch(getInterestRequest(data))
    }
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LoanSelector)
