import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types'
import Section from "@components/Section";
import SliderComponent from "@components/Slider";
import useDebounce from "../../utils/useDebounce";
import ContentBox from "@components/ContentBox";

const LoanSelector = ({ handleGetInterestRequest, interest : { data } }) => {
    const { monthlyPayment, totalInterest } = data;
    const [loanDetails, setLoanDetails] = useState({
        borrowingAmount: 1000,
        noOfMonths: 6
    });
    //wait 500ms before firing this event
    const debouncedLoanDetails = useDebounce(loanDetails, 500);
    const handleOnChangeSlider = (sliderName, sliderVal) => {
        setLoanDetails(prevState => ({
            ...prevState, [sliderName]:sliderVal
        }))
    };
    useEffect(() => {
        if(debouncedLoanDetails) {
            handleGetInterestRequest(debouncedLoanDetails)
        }
    }, [debouncedLoanDetails, handleGetInterestRequest]);
    return (
        <ContentBox>
            <Section>
                <h2>How much do you want to borrow?</h2>
                <SliderComponent
                    name="borrowingAmount"
                    min={1000}
                    max={5000}
                    onChange={(e, number) => handleOnChangeSlider('borrowingAmount', number)}/>
            </Section>
            <Section>
                <h2>For how many months?</h2>
                <SliderComponent
                    name="noOfMonths"
                    min={6}
                    max={36}
                    step={1}
                    onChange={(e, number) => handleOnChangeSlider('noOfMonths', number)}/>
            </Section>
            <Section>
                <h2>Results</h2>
                <p>Interest: £{totalInterest.amount}</p>
                <p>Monthly Repayments: £{monthlyPayment.amount}</p>
            </Section>
        </ContentBox>
    );
};

LoanSelector.propTypes = {
    handleGetInterestRequest: PropTypes.func,
    interest: PropTypes.object,
};

export default LoanSelector;
