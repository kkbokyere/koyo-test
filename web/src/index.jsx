import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import rootReducer from './reducers'
import { createStore, applyMiddleware } from 'redux'
import { all } from 'redux-saga/effects'

import createSagaMiddleware from 'redux-saga'

import { composeWithDevTools } from 'redux-devtools-extension';

import '@styles/index.scss';
import App from './App';
import * as serviceWorker from './serviceWorker';
import * as sagas from '@sagas'


// create the saga middleware
const sagaMiddleware = createSagaMiddleware();

// Redux store
const store = createStore(
    rootReducer,
    composeWithDevTools(applyMiddleware(sagaMiddleware))
);

function * rootSagas () {
    yield all(sagas)
}

sagaMiddleware.run(rootSagas);
ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
