import React from 'react';
import PropTypes from 'prop-types'
import styles from './Section.module.scss'

const Section = ({ children }) => {
    return (
        <section className={styles.container}>
            {children}
        </section>
    );
};

Section.propTypes = {
    children: PropTypes.any,
};

export default Section;
