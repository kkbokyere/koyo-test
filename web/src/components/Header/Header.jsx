import React from 'react';
import logo from "@root/logo.svg";
import styles from './Header.module.scss'
const Header = () => {
    return (
        <header className={styles.header}>
            <img src={logo} className={styles.headerLogo} alt="Koyo Logo" />
        </header>
    );
};

export default Header;
