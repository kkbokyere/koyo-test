import React from 'react';
// import styles from './Slider.module.scss'
import { makeStyles } from '@material-ui/core/styles';
import Slider from '@material-ui/core/Slider';

const useStyles = makeStyles({
    root: {
        color: '#52af77',
        height: 20,
    },
    thumb: {
        height: 24,
        width: 24,
        backgroundColor: '#fff',
        border: '2px solid currentColor',
        marginTop: -8,
        marginLeft: -12,
        '&:focus, &:hover, &$active': {
            boxShadow: 'inherit',
        },
    },
    active: {},
    valueLabel: {
        left: 'calc(-50% + 4px)',
    },
    track: {
        height: 8,
        borderRadius: 4,
    },
    rail: {
        height: 50,
        borderRadius: 4,
    },
});

const SliderComponent = ({ ...props }) => {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <Slider
                {...props}
                defaultValue={0}
                valueLabelDisplay="auto"
                />
        </div>
    );
};

export default SliderComponent;
