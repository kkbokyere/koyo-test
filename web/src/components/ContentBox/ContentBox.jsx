import React from 'react';
import PropTypes from 'prop-types'

import styles from './ContentBox.module.scss'

const ContentBox = ({ children }) => {
    return (
        <div className={styles.container}>
            {children}
        </div>
    );
};


ContentBox.propTypes = {
    children: PropTypes.any,
};

export default ContentBox;
