import React from 'react';
import '@styles/App.scss';
import Layout from "@components/Layout";
import Header from "@components/Header";
import LoanSelector from "@containers/LoanSelector";

function App() {
  return (
    <div className="App">
        <Layout>
            <Header/>
            <LoanSelector/>
        </Layout>
    </div>
  );
}

export default App;
