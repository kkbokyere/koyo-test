import { combineReducers } from 'redux'
import interest from './interest'
export default combineReducers({
    interest
})
