import types from '@constants/action-types';

const initialState = {
    isLoading: false,
    error: null,
    data: {
        totalInterest: {
            amount: ''
        },
        monthlyPayment: {
            amount: ''
        }
    }
};

export default (state = initialState, action) => {
    let { payload } = action;

    switch (action.type) {
        case types.GET_INTEREST_SUCCESS:
            return {
                ...state,
                data: payload,
                isLoading: false
            };
            case types.GET_INTEREST_FAILURE:
            return {
                ...state,
                isLoading: false,
                error: payload
            };
            case types.GET_INTEREST_REQUEST:
            return {
                ...state,
                isLoading: true
            };
        default:
            return state
    }
}
