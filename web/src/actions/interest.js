import types from "@constants/action-types";

export function getInterestRequest(payload) {
    return {
        type: types.GET_INTEREST_REQUEST,
        payload
    }
}

export function getInterestSuccess(payload) {
    return {
        type: types.GET_INTEREST_SUCCESS,
        payload
    }
}

export function getInterestFailure(payload) {
    return {
        type: types.GET_INTEREST_FAILURE,
        payload
    }
}
